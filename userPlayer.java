/**********************************************************
 *   public class userPlayer                              * 
 *   This class contains information the human player.    *
 *   It inherits from the Player class which interfaces   * 
 *   with the Hand class. Contains the makeMove function  * 
 *   which will prompt the user for which cards it wants  * 
 *   to discard. It then validates the discard and        * 
 *   continues.                                           * 
 **********************************************************
 *   UIC CS 342 Software Design, Spring 2014              *  
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/

import java.util.Scanner;


public class userPlayer extends Player {
    /* public constructor calls the super constuctor */
	public userPlayer(){super();}

    /* boolean validateDiscard(String discard[], int aceIndex)
	 * Returns false if user doesn't enter a valid number, tries to discard too many cards, 
	 * tries to discard 4 cards INCLUDING their ace.  Returns true on all other valid discards.*/
	private boolean validateDiscard(String discard[], int aceIndex){
		if(discard.length == 1 && discard[0].equals(""))
			return true;
		for(int i=0; i<discard.length; i++){
			try{
				Integer.parseInt(discard[i]);
			}catch(NumberFormatException nfe){
				System.out.println("ERROR: "+discard[i]+" is not a number.");
				return false;
			}
			
			if(Integer.parseInt(discard[i])<1 || Integer.parseInt(discard[i])>5){
				System.out.println(discard[i]+" is not a valid position.");
				return false;
			}
		}
	
		if(discard.length == 4){
			if(aceIndex == -1){
				System.out.println("You can only discard at most three cards.");
				return false;
			}
			for(int i=0; i<discard.length; i++){
				if (Integer.parseInt(discard[i])-1 == aceIndex){
					System.out.println("You must keep your ace if you want to discard four cards.");
					return false;
				}
			}
		}
		
		if(discard.length > 4){
			System.out.println("You can't discard that many cards.");
			return false;
		}
		
		return true;
	}//end validateDiscard
    

	/* void makeMove(Deck deck) 
	 * asks the user which cards to discard and then validates 
	 * their input and does the appropriate discard. */
    public void makeMove(Deck deck){
    	Scanner input = new Scanner(System.in);
    	System.out.println("It is your turn to discard and draw new cards, if you wish.\n");
		
		String[] discard;
		int aceIndex;
		do{
			hand.displayHand();
			aceIndex = hand.hasAce();
			if(aceIndex != -1)
				System.out.println("Since you have an Ace you can keep the Ace and discard the other four cards.");
			System.out.println("List the card numbers you would like to discard separated by spaces: ");
			String discardString = input.nextLine();
			discard = discardString.split(" ");	
		}while(!validateDiscard(discard, aceIndex));
		
		
		for(int i=0; i<discard.length; i++){
			if(!discard[i].equals(""))
				discardAndReplaceCard(Integer.parseInt(discard[i])-1, deck.dealCard());
		}
		
		input.close();
    	
    }//end makeMove()
    
}//end class userPlayer
