/**********************************************************
 *   public class HandValue                               * 
 *   This class contains all information that is needed   *
 *   in order to break any ties and to evaluate hands.    * 
 *   it is the value that is returned from the compare    * 
 *   hand method in the hand class and it keeps info      * 
 *   about the different types of matches and another     * 
 *   copy of the sorted hand. The other copy is needed    * 
 *   in order for this class to be used in the opponent   * 
 *   class.                                               *
 **********************************************************  
 *   UIC CS 342 Software Design, Spring 2014              * 
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/
public class HandValue {
	private int straightMax; 
	private int matchCount1Rank, matchCount2Rank; 
	private int value; 
	public Card [] hand; 
	/* HandValue constructor */
	public HandValue(int rank1, int rank2, int str8Max, int handval, Card[] hand){
		matchCount1Rank = rank1;
		matchCount2Rank = rank2;
		straightMax = str8Max;
		value = handval;  
		this.hand = hand; 
	}//end HandValue constructor 

	/* int getHandValue() 
	 * returns the value of the hand. values range from 0-8 to indicate
	 * high card -> straight flush 
	 */ 
	public int getHandValue(){return value;}
	
	/* int getMatchRank(int i) 
	 * returns rank for the first match or the second match 
	 * depending on what i is, returns -1 otherwise 
	 * used to compare hands for the AI 
	 */
	public int getMatchRank(int i) {
		if(i==1) return matchCount1Rank; 
		if(i==2) return matchCount2Rank; 
		else return -1; 
	}//end getMatchRank
	
	/* static int compareHands(HandValue[], int numHands) 
	 * returns the index of the winning hand. This is a static method 
	 * because it does not need to be attached to an instance when called 
	 * rather it is given the instances to compare
	 */
	public static int compareHands(HandValue[] hands, int numHands){
		int max=0; 
		boolean tie = false; 
		int tiedHands[] = new int[numHands];
		int numTies=0;
		int winner=0; 
		
		//first we walk through the hand values and find the biggest one 
		//noting if a tie occurs. 
		for(int i=0; i<numHands; i++) tiedHands[i]=0; 
		for(int i=0; i<numHands; i++){ 
			if(hands[i].value == max){
				tie = true;
				tiedHands[numTies++] = i; 
			}
			else if(hands[i].value > max){
				tie = false; 
				numTies = 0;
				tiedHands[numTies++] = i; 
				max = hands[i].value;
				winner = i; 
			}
		}
		
		//there are no ties, so whoever had maxvalue won 
		if(!tie){ return winner; }
		
		//figure out what they're tied for 
		int handvalue = hands[tiedHands[0]].value;
		switch(handvalue){
		//if theyre tied with a highcard or a flush, we simply compare the top ranks
		//for all the hands and return the best one.   
		case 0:
		case 5: 
			max = -1;
			tie = false;
			 
			for(int j=0; j<5; j++){ //we know there will only be up to 5 possible ties. 
				int numTies2 = 1; 
				int arr[] = new int[5];
				for(int i=0; i<numTies; i++){
					if((hands[tiedHands[i]].hand[j].getRank().ordinal() == max)){
						tie = true; 
						arr[numTies2] = tiedHands[i];
						numTies2++; 
					}
					else if (hands[tiedHands[i]].hand[j].getRank().ordinal() > max){
						max = hands[tiedHands[i]].hand[j].getRank().ordinal(); 
						winner = tiedHands[i]; 
						tie = false;
						numTies2 = 1; 
						arr[0] =tiedHands[i]; 
						
					}	
				}
				if(!tie) break; 
				else if(j==4){return -1; }
				else{
					tie = false; 
					numTies = numTies2; 
					tiedHands = arr; 
				}
			}
			return winner; 
			
		//if theyre tied with a one pair, then we compare matchCount1Ranks for the tied hands 
		case 1:
			max = -1; 
			for(int i=0; i<numTies; i++)
				if (hands[tiedHands[i]].matchCount1Rank == 1){
					max = 14; 
					winner = tiedHands[i];
				}
				else if(hands[tiedHands[i]].matchCount1Rank > max){
					winner = tiedHands[i];
					max = hands[tiedHands[i]].matchCount1Rank;
				}
			return winner; 
		
		//if they're tied with a two-pair, we compare both match1 and match2 for the tied hands. 
		case 2:
			max = -1; 
			for(int i=0; i<numTies; i++){
				if(hands[tiedHands[i]].matchCount1Rank == 1){
					winner = tiedHands[i]; 
					max = 14;
				}
				else if(hands[tiedHands[i]].matchCount1Rank > max){
					winner = tiedHands[i]; 
					max = hands[tiedHands[i]].matchCount1Rank;
				}
				
				if(hands[tiedHands[i]].matchCount2Rank == 1){
					winner = tiedHands[i];
					max = 14;
				}
				else if(hands[tiedHands[i]].matchCount2Rank > max){
					winner = tiedHands[i];
					max = hands[tiedHands[i]].matchCount2Rank;
				}
			}
			return winner; 
		
		//if theyre tied with a 3,4 of a kind 
		//we compare the matchCount1Ranks 
		case 7:
		case 3:
			max = -1; 
			for(int i=0; i<numTies; i++)
				if(hands[tiedHands[i]].matchCount1Rank == 1){
					winner = tiedHands[i];
					max = 14;
				}
				else if(hands[tiedHands[i]].matchCount1Rank > max){
					winner = tiedHands[i];
					max = hands[tiedHands[i]].matchCount1Rank;
				}
			return winner; 
			
		//for full house we compare only matchCount2 (because that has the 3staright),
		case 6: 
			max = -1; 
			for(int i=0; i<numTies; i++)
				if(hands[tiedHands[i]].matchCount2Rank == 1){
					winner = tiedHands[i];
					max = 14;
				}
				else if(hands[tiedHands[i]].matchCount2Rank > max){
					winner = tiedHands[i];
					max = hands[tiedHands[i]].matchCount2Rank;
				}
			return winner; 
			
		//if theyre tied with a straight or a straight flush, we compare the straightMax
		case 8:
		case 4:
			max = -1;
			tie = false; 
			for(int i=0; i<numTies; i++)
				//the straight max is already set up to account for Ace being a high or a low card 
				if(hands[tiedHands[i]].straightMax == max) tie = true; 
				else if(hands[tiedHands[i]].straightMax > max){
					winner = tiedHands[i];
					max = hands[tiedHands[i]].straightMax;
				}
				else if(hands[tiedHands[i]].straightMax == 1){
					winner = tiedHands[i];
					max = 14; 
				}
			if(!tie ) return winner; 
			else return -1;
		
			//should never get here so fail
		default:
			System.out.println("WuhhhhhtttT?!?!?!?!?!");
			return winner; 
		} 
	}//end compareHands 

	
}//end HandValue class 
