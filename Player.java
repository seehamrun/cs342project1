/**********************************************************
 *   public class Player                                  * 
 *   This class contains information for the players to   * 
 *   interact with the deck of cards. Since the code is   * 
 *   the same regardless if the player is a human or a    * 
 *   CPU, it only made sense to put them into a shared    * 
 *   class and have them both inherit these methods       * 
 *   (instead of copy-pasting them into both classes      * 
 **********************************************************
 *   UIC CS 342 Software Design, Spring 2014              *  
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/
public class Player {
	protected Hand hand; //Hand of 5 cards
	
	/* Player constructor */
	public Player(){hand = new Hand();}
	
	//these functions dont do anything but call the corresponding functions in the hand class
	//i thought that since they dealt directly with the hand, it'd be better to have them there 
	//and not in the player class. 
	public void insertCard(Card card){hand.insertCard(card);}
	public void displayHand(){ hand.displayHand();}
	public void discardAndReplaceCard(int i, Card card){ hand.discardAndReplaceCard(i, card); }
	public HandValue evaluateHand(){return hand.evaluateHand(true);	} //print true because we want to print stuff 
}//end Class Player 