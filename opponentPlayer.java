/**********************************************************
 *   public class opponentPlayer                          * 
 *   This class contains information the CPU player.      *
 *   It inherits from the Player class which interfaces   * 
 *   with the Hand class. The only other function is the  * 
 *   makeMove function which, depending on the state of   * 
 *   the users hand, will discard the appropriate cards   *   
 **********************************************************
 *   UIC CS 342 Software Design, Spring 2014              *  
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/

public class opponentPlayer extends Player {
	/* opponent constructor just calls the super constructor */
    public opponentPlayer(){super();}
    
    /* public int makeMove(Deck deck) 
     * discards and replaces cards based on the state of the hand 
     * at the time of the call. 
     * returns the number of cards that the AI discarded 
     * */
    public int makeMove(Deck deck){
    	HandValue handval = hand.evaluateHand(false); 
    	int value = handval.getHandValue();
    	/*System.out.println("before discarding from hand: ");
    	hand.displayHand();*/
    	
    	int numDiscards = 0; 
    	
    	//check if comp has one pair or better 
    	//if so, discard all other cards
    	//one pair. or 3 of a kind, or 4 of a kind check matchCount1 
    	if(value == 1 || value == 3 || value == 7){
    		for(int i=0; i<5; i++)
    			if(handval.hand[i].getRank().ordinal()+1 != handval.getMatchRank(1)){
					hand.discardAndReplaceCard(i, deck.dealCard());
					numDiscards ++; 
    			}
    		/*System.out.println("after discarding from hand in one-pair, three-of-a-kind: ");
        	hand.displayHand();	
        	System.out.println();
        	*/ 
    		return numDiscards; 
    	}
    	
    	//two pair throw out the mismatching card. 
    	else if(value == 2 ){
    		for(int i=0; i<5; i++)
    			if(handval.hand[i].getRank().ordinal()+1 != handval.getMatchRank(1) && 
    			   handval.hand[i].getRank().ordinal()+1 != handval.getMatchRank(2)){
					hand.discardAndReplaceCard(i, deck.dealCard());
					numDiscards ++; 
    			}
    		/*System.out.println("after discarding from hand in two-pair: ");
        	hand.displayHand();	
        	System.out.println(); 	
    		*/
    		return numDiscards; 
    	}
    	//in a full house, straight, straight flush, or flush, keep everything
    	else if (value == 6 || value==8 || value==5 || value==4) return 0; 
    	
    	
    	//if they have a high card, figure out if they have 4 of same suit.
    	//discard the different suit card 
    	if(value==0){
    		int suitCount=0; 
    		int suit=handval.hand[0].getSuit().ordinal(); 
    		for	(int i=0; i<5; i++){
    			int matches=0; 
	    		for(int j=0; j<5; j++){
	    			if(handval.hand[i].getSuit().ordinal() == handval.hand[j].getSuit().ordinal())
	    				matches++; 
	    		}
	    		if(matches > suitCount) {
	    			suitCount = matches; 
	    			suit = handval.hand[i].getSuit().ordinal(); 
	    		}
    		}
    		
    		//figure out which is different and discard it. 
    		if(suitCount==4){
    			for(int i=0; i<5; i++)
    				if(handval.hand[i].getSuit().ordinal()!=suit){
    					hand.discardAndReplaceCard(i, deck.dealCard());
    					numDiscards++; 
    				}
    			/*System.out.println("after discarding from hand high card with 4 same suit.: ");
            	hand.displayHand();	
            	System.out.println();
            	*/ 
    			return numDiscards; 
    		}
    	}
    	
    
    	//if they have 4 in sequence
    	//discard the out of sequence one 
    	//since they are in order, we know the sequence is either going to be positions 
    	//0 1 2 3 or 1 2 3 4 or  
    	boolean sequence = true;
    	int endcard=-1; 
    	for(int i=0; i<3; i++){
    		if(handval.hand[i].getRank().ordinal() != handval.hand[i].getRank().ordinal() +1 )
    			sequence = false;
    		endcard = 3; 
    	}
    	if(!sequence){
    		sequence = true;
    		for(int i=1; i<4; i++)
        		if(handval.hand[i].getRank().ordinal() != handval.hand[i].getRank().ordinal() +1 )
        			sequence = false; 
    		endcard = 4; 
    	}
    	
    	if(sequence){
    		hand.discardAndReplaceCard(endcard+1, deck.dealCard());
    		/*System.out.println("after discarding from hand in sequence: ");
        	hand.displayHand();	
        	System.out.println(); 
    		*/
    		return 1; 
    	}
    	
    	
    	//if they have an ace
    	//discard the other 4 cards 
    	int AceIndex = hand.hasAce(); 
    	if(AceIndex!=-1){
    		for(int i=0; i<5; i++){
    			if(i!=AceIndex){
    				hand.discardAndReplaceCard(i, deck.dealCard());
    				numDiscards ++; 
    			}
    		}
    		/*System.out.println("after discarding from hand in has Ace: ");
        	hand.displayHand();	
        	System.out.println(); 
    		*/
    		return numDiscards; 
    	}
    	
    	
    	//else keep 2 highest
    	for(int i=2; i<5; i++){
    		hand.discardAndReplaceCard(i, deck.dealCard());
    		numDiscards++; 
    	}
    	/*System.out.println("after discarding from hand in 2 highest: ");
    	hand.displayHand();	
    	System.out.println(); 
    	*/
    	return numDiscards; 
    	
    }//end makeMove()
    
}//end class opponentPlayer
