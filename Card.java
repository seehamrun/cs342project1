/**********************************************************
 *   public class Card                                    * 
 *   This class is used to track information about each   * 
 *   card such as it's rank and its Suit. This info is    *
 *   typed as a final variable since after it's set, the  * 
 *   value won't change.                                  *   
 **********************************************************
 *   UIC CS 342 Software Design, Spring 2014              *  
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/
public class Card {
    public enum Suit{ C, D, H, S }
    public enum Rank{
    	Two("2"), Three("3"), Four("4"), Five("5"), 
		Six("6"), Seven("7"), Eight("8"), Nine("9"), 
		Ten("T"), Jack("J"), Queen("Q"), King("K"), Ace("A");
		
		private String rank;
		
		Rank(String rank){ this.rank = rank; }
		
		String getSymbol(){ return this.rank; }
    }
    
    private final Suit suit;
    private final Rank rank;
    
    /* public constructor */
    public Card(Rank rank, Suit suit){
    	this.rank = rank;
    	this.suit = suit;
    }//end constructor 
    
    /* needed getter functions */
    public Rank getRank(){ return rank; }
    public Suit getSuit(){ return suit; }
    public String toString(){ return rank.getSymbol() + suit; }

}//end class Card 
