/**********************************************************
 *   public class Hand                                    * 
 *   This hand contains information about 5 cards. These  * 
 *   cards are the ones that are in the players hand.     * 
 *   This class also contains an EvaluateHand() method    * 
 *   that returns the handValue associated to the hand at * 
 *   the point of the call.                               *
 **********************************************************
 *   UIC CS 342 Software Design, Spring 2014              *  
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/

public class Hand {
	private Card[] hand;
	private int numCards; 
	
	/* class constructor */
	public Hand(){
		hand = new Card[5];
		numCards = 0; 
	}//end Hand constructor 
	
	/* void insertCard (Card card)
	 * inserts the given card into the hand, used when  
	 * Initially handing out cards in order to increment the card count.*/
	public void insertCard(Card card){
		hand[numCards]=card;
		numCards ++; 
	}//end insertCard
	
	/* void discardAndReplaceCard(int i, Card card) 
	 * overwrites the ith card in hand with the card specified 
	 * if this was C, we'd have to worry about deallocating the card 
	 * first, but we'll let the garbage collector take care of that for us. ;D */
	public void discardAndReplaceCard(int i, Card card){
		hand[i]  = card; 
	}//end discardAndReplaceCard 
	
	/* void sortHand()
	 * Uses insertion sort to sort the hand by ranks 
	 * gets invoked before comparingHands in order to compare by ranks */
	private void sortHand(){ 
		for(int i=0; i<5; i++){
			for(int j=i; j>0; j--){
				if(hand[j-1].getRank().ordinal() < hand[j].getRank().ordinal()){
					Card temp = hand[j];
					hand[j] = hand[j-1];
					hand[j-1] = temp;
				}	
			}
		}		
	}//end sortHand
	
	
	
	/* This function will return an integer as the index of the Ace if the player's hand. 
	 * If the player does not have an Ace, it will return -1.*/
	public int hasAce(){
		for(int i=0; i<5; i++){
			if(hand[i].getRank().getSymbol() == "A")
				return i;
		}
		return -1;
	}
	
	/* void sortHandByMatches()
	 * sorts hand by ranks and then by matches
	 * this is invoked at the beginning of every 
	 * displayHand() function */
	private void sortHandByMatches(){
        int matchCount[] = new int[5];
        for(int i=0; i<5; i++) matchCount[i]=0;
        //sort by ranks first 
        sortHand(); 
        //get match counts
        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++)
                if(hand[i].getRank().ordinal()==hand[j].getRank().ordinal()){
                    matchCount[i]++;
                }
        }

        //sort by match counts
        for(int i=0; i<5; i++){
			for(int j=i; j>0; j--){
				if(matchCount[j-1] < matchCount[j]){
					int tempCount = matchCount[j]; 
					matchCount[j] = matchCount[j-1]; 
					matchCount[j-1] = tempCount; 
					
					Card temp = hand[j];
					hand[j] = hand[j-1];
					hand[j-1] = temp;
				}	
			}
		}
	}//end sortHandByMatchCounts
	
	/* void displayHand 
	 * This functions will sort the hand based on 
	 * matchCounts and rank and then display */
	public void displayHand(){
		sortHandByMatches();//sort the hand first before displaying it. 
		for(int i =0; i<5; i++)
			System.out.print(i+1 + ". " + hand[i] + "\t");
		System.out.println();
	}//end displayHand

	
	/* String getRank(int rank) 
	 * returns the string representation of the rank given 
	 * This is used to print out what the rank is for the card in string form 
	 * I couldn't figure out how to use the enum for this so i just didn't bother...*/
	private String getRank(int rank){
		switch(rank){
			//case 1:	return "Ace"; 
			case 2: return "Two";
			case 3: return "Three"; 
			case 4: return "Four";
			case 5: return "Five" ; 
			case 6: return "Six";
			case 7: return "Seven"; 
			case 8: return "Eight"; 
			case 9: return "Nine"; 
			case 10:return "Ten"; 
			case 11:return "Jack"; 
			case 12:return "Queen"; 
			case 13:return "King";
			case 14:return "Ace";
			default:return null; 	
		}
	}//end getRank
	
	/* HandValue evaluateHand() 
	 * Evaluates the hand given and returns a HandValue instance 
	 * This instance contains all the information needed to figure out 
	 * who won when comparing it with other hands
	 * It also prints out the type of hand it is. */
	public HandValue evaluateHand(boolean print) {
		sortHand(); 
		sortHandByMatches();
		int ranks[] = new int[15]; 
		for (int i=0; i<15; i++){
			ranks[i]=0; 
		}
		for(int i=0; i<5; i++)
			ranks[hand[i].getRank().ordinal()+2] ++; //2 is 0 ... Ace is 12...
													 //so we add 2 to make 2..14 
		int matchCount1 = 1;
		int matchCount2 = 1;
		int matchCount1Rank = 0; 
		int matchCount2Rank = 0; 
		for(int i=2; i<15; i++){
			if(ranks[i]>matchCount1){
				if (matchCount1 != 1){//if matchCount was not the default value
				    matchCount2 = matchCount1;
				    matchCount2Rank = matchCount1Rank;
			    }
		        matchCount1 = ranks[i];
		        matchCount1Rank = i;
			}
			else if (ranks[i]>matchCount2){
				matchCount2 = ranks[i];
		        matchCount2Rank = i;
			}
		}
		
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * at this point in our code we know if there are 1 or 2 "matches"   * 
		 * we also know if we have a 3 of a kind or a 4 of a kind.           *
		 * matchCount1 has first count match (3 of a kind, 4 of a kind)      *
		 * matchCount2 has the second matchCount (if we have a 2pair or a    *
		 * full house 														 *  
		 * Now we need to figure out straights and flushes.                  * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */ 
				
		//straights can be determined by going through our rank counts. 
		//we need to find 5 consecutive ranks. 
		int straightValue = 0; 
		boolean straight = false; 
		//test for straight	
		if (hand[0].getRank().ordinal() == hand[1].getRank().ordinal()+1 && 
			hand[1].getRank().ordinal() == hand[2].getRank().ordinal()+1 &&
			hand[2].getRank().ordinal() == hand[3].getRank().ordinal()+1 &&
			hand[3].getRank().ordinal() == hand[4].getRank().ordinal()+1){
				straight=true;
				if(hand[0].getRank().getSymbol() == "A")
					straightValue = 16;	//because a straight of A-K-Q-J-T should beat A-2-3-4-5 and K-Q-J-T-9
				else
					straightValue=hand[0].getRank().ordinal() + 2 ; //so we get the actual number and not 0..11 for 2-K 
		}
		//test for special case of a straight A-5-4-3-2
		else if(hand[0].getRank().getSymbol() == "A" &&
				hand[1].getRank().getSymbol() == "5" &&
				hand[2].getRank().getSymbol() == "4" &&
				hand[3].getRank().getSymbol() == "3" &&
				hand[4].getRank().getSymbol() == "2"){
					straight = true;
					straightValue = 1;	//this is the least desireable straight
		}
		
		//flushes occur if all 5 are same suit. 
		boolean flush = true; //assume that there is flush unless we find 2 cards that are not the same suite 
		for(int i=0; i<4; i++)
			if(hand[i].getSuit() != hand[i+1].getSuit())
				flush = false; 
		
		int value; 
		//straight flush 
		if (straight && flush ) {
			if(print)System.out.println("You have a straight flush to the " + hand[0].getRank()+".");
			value = 8; 
		}
		//four of kind
		else if(matchCount1==4){
			if(print)System.out.println("You have a Four-Of-A-Kind(" + getRank(matchCount1Rank)+"'s).");
			value =  7; 
		}
		//also sure that this is unnneccesary 
		else if(matchCount2==4){
			if(print)System.out.println("You have a Four-Of-A-Kind(" + getRank(matchCount2Rank)+"'s).");
			value = 7; 
		}
		//full house 
		//pretty sure the second part of the or is unneccessary 
		else if( (matchCount1==3 && matchCount2==2) || (matchCount2==3 && matchCount1==2)){
			if(print)System.out.println("You have a full house(" + getRank(matchCount1Rank) + "'s and " + getRank(matchCount2Rank)+"'s).");
			value = 6; 
		}
		//flush 
		else if (flush){
			if(print)System.out.println("You have a flush(" + hand[0].getSuit()+").");
			value =  5; 
		}
		//straight
		else if(straight){
			if(print)System.out.print("You have a straight to the "+hand[0].getRank()+".\n"); 
			value =  4; 
		}
		//three of a kind
		else if (matchCount1==3){
			if(print)System.out.println("You have a Three-Of-A-Kind(" + getRank(matchCount1Rank)+"'s).");
			value =  3; 
		}
		//two pair
		else if (matchCount1 == 2 && matchCount2==2){
			if(print)System.out.println("You have Two-Pair(" + getRank(matchCount1Rank) + "'s and " + getRank(matchCount2Rank)+"'s).");
			value =  2; 
		}
		//one pair 
		else if (matchCount1 == 2 && matchCount2 == 1){
			if(print)System.out.println("You have a Pair(" + getRank(matchCount1Rank)+"'s).");
			value = 1;
		}
		//high card
		else{
			if(print)System.out.println("You just have a High Card("+hand[0].getRank()+").");
			value =  0; 
		}		
		
		System.out.println(); 
		
		sortHand(); 
		return new HandValue(matchCount1Rank, matchCount2Rank, straightValue, value, hand);
		
	}//end evaluateHand()....
	
}//end class Hand
