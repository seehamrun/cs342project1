/**********************************************************
 *   public class Deck                                    * 
 *   Contains and array of 52 cards in order to           * 
 *   emulate a "deck". This contains things such as       * 
 *   shuffling and dealing cards                          * 
 **********************************************************
 *   UIC CS 342 Software Design, Spring 2014              *  
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/
public class Deck {
    private Card[] deck;		//Array of 52 cards
    private int cardsUsed;		//Keep track of how many cards have already been used
    
    /* Public Constructor initializes all 52 cards */
    public Deck(){
		deck = new Card[52];
		int cardCount = 0;
		for(Card.Suit suit : Card.Suit.values()){
		    for(Card.Rank rank : Card.Rank.values()){
			deck[cardCount] = new Card(rank, suit);
			cardCount++;
		    }
		}	
		cardsUsed = 0;
    }//end constructor 
    
    /* void shuffle 
     * shuffles all cards by just randomly picking a card 
     * to switch with */
    public void shuffle(){
		for(int i = 0; i<deck.length; i++){
		    int random = (int)(Math.random()*deck.length);
		    Card temp = deck[i];
		    deck[i] = deck[random];
		    deck[random] = temp;
		}
		cardsUsed = 0; 		//set cardsUsed to zero since we're only shuffling on a new deck
    }//end shuffle 
    
    /* Card dealCard 
     * returns the next unused card unless the deck is finished
     * */
    public Card dealCard(){
		if(cardsUsed == deck.length)
		    throw new IllegalArgumentException("Deck is out of cards!\n");
		cardsUsed++;
		return deck[cardsUsed-1];
    }//end dealCard
    
}//end class Deck 
