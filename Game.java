/**********************************************************
 *   public class Game                                    * 
 *   This class contains the main method.                 *
 *   The only thing it does is ask the user information   *
 *   about the game and then starts game play             *    
 **********************************************************
 *   UIC CS 342 Software Design, Spring 2014              *  
 *   Project 1 - Poker                                    *
 *   Siham Hussein, James Klonowski                       *
 **********************************************************/

import java.util.Scanner;
public class Game {
	/* main method, asks the user for set up information 
	 * (number of players, etc and initializes gamePlay */
	public static void main(String args[]){
		int numAI = 0;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Welcome to Poker!\n");

		/* setting up game-play and initializing Players */ 
		Deck deck = new Deck();
		
		do{	
			System.out.println("How many computer players will be playing(1-3)? ");
			//numAI = input.nextInt();
			String numAIinput;
			numAIinput = input.next();
			try{
				numAI = Integer.parseInt(numAIinput);
			}catch(NumberFormatException nfe){
					System.out.println("ERROR: "+numAIinput+" is not a number.");
				}
			if(numAI > 3 || numAI < 1)
				System.out.println("ERROR: There must be 1-3 computer players.\n");
		}while(numAI > 3 || numAI < 1);
				
		userPlayer user = new userPlayer();
		opponentPlayer[] AI = new opponentPlayer[numAI];
		for(int i=0; i<numAI; i++){
			AI[i] = new opponentPlayer();
		}
		
		
		System.out.println("Cards are now being shuffled.\n");
		deck.shuffle();
		
		System.out.println("Cards are now being dealt to "+numAI+" computer opponent(s) and 1 human player.\n");
		for (int i=0; i<5; i++){
			user.insertCard(deck.dealCard());
			for(int j=0; j<numAI; j++){
				AI[j].insertCard(deck.dealCard());
			}
		}
		
		//array to store the results of the user's hand and the result of the AI's hand
		//so this way, the evaulateHand method will return an int for the "type of hand" 
		//that the user/AI had and we can use that to figure out who won (by finding the max) 		
		HandValue results[]=new HandValue[numAI+1];
		
		//Prompt user to make their move 	
		user.makeMove(deck);
		//make move for the AI's one by one. 
		for(int i = 0; i<numAI; i++){
			int numDiscards = AI[i].makeMove(deck);
			System.out.println("Computer player " + (i+1) + " discarded " + numDiscards + " cards.");
		}
		System.out.println("\nYour final hand is:");
		user.displayHand();
		results[0]=user.evaluateHand();
		for(int i=0; i<numAI; i++){
			System.out.println("Computer player #"+(i+1)+"'s hand:");
			AI[i].displayHand();
			results[i+1]= AI[i].evaluateHand();
		}
		
		int winner = HandValue.compareHands(results, numAI+1);
		if(winner < 0) System.out.println("uh oh... there was an unresolved tie");
		else if(winner == 0) System.out.println("Congrats You Win!!!!!!!"); 
		else System.out.println("Computer #" + winner + " Wins!!!");
		
		System.out.println();
		input.close();
	}//end main()
	
}//end class Game
	